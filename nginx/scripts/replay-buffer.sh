#!/bin/sh

echo "Creating a replay buffer for ${1}"
# mkdir -p /var/replaybuffer
mkdir -p /var/replaybuffer/live/${1}
ffmpeg -i rtmp://localhost:1935/livereplay/${1} -f segment -segment_time 5 -segment_wrap 12 -segment_list /var/replaybuffer/live/${1}/${1}.m3u8 -segment_list_size 12 /var/replaybuffer/live/${1}/seg-${1}-%d.ts
