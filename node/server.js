const {
    lstatSync,
    readdirSync
} = require('fs')
const {
    join
} = require('path')
const exec = require('child_process').exec;
const express = require('express')
const app = express()
const port = 3000

app.get('/', async (req, res) => {
    let body = `<html><body><h1>Sources ready for replay</h1><ul>`;

    const isDirectory = source => lstatSync(source).isDirectory()
    const getDirectories = source =>
        readdirSync(source).map(name => join(source, name)).filter(isDirectory).map((dir) => dir.replace(source, ""));

    try {
        const dirs = getDirectories("/var/replaybuffer/live");

        dirs.forEach((dir) => {
            body += `<li>${dir}</li>`
        });
    } catch (e) {
        body +=  `<h2 style="color:darkred;">No sources found.</h2>`;
    }

    body += `</ul></body></html>`;

    res.send(body);
});

app.get('/api/generateReplay', async (req, res) => {
    exec('sh ./scripts/save-replays.sh',
        (error, stdout, stderr) => {
            if (stderr || error) {
                res.status(500);
                res.send(JSON.stringify({
                    stderr,
                    error
                }))
            } else {
                res.status(200);
                res.send(`OK \nlast replay : ${new Date().toLocaleString()}`);
            }
        });
});

app.listen(port, () => {
    console.log(`Server ready, listening on ${port}`)
})