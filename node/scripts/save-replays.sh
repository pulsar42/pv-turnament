#!/bin/sh

BASE_PATH=/var/replaybuffer/
EXISTING_REPLAYS=$((`stat -c %h ${BASE_PATH}replays/old` - 2))
NEW_REPLAY=${BASE_PATH}replays/old/${EXISTING_REPLAYS}-replay

mkdir -p ${NEW_REPLAY}

rm -rf ${BASE_PATH}replays/latest/*

echo "Saving all replay buffers"
for streamName in ${BASE_PATH}live/*/; do
    echo "saving $streamName";

    # Make sure we have a latest folder
    mkdir -p ${BASE_PATH}replays/latest/
    # Copy the content of all our streams to it
    cp $streamName/* ${BASE_PATH}replays/latest/

    # And keep it on another folder for back-up purposes
    cp -R $streamName ${NEW_REPLAY}
done